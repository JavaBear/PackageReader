import java.io.File;
import java.util.ArrayList;

public class PackageReader {

    public ArrayList<String> getFiles (String folderName) {

        // Проверка, есть ли что-то в переданной строке
        if (folderName == null) {
            return null;
        } else if (folderName.replaceAll("\\s","").isEmpty()){
            return null;
        }

        // Создание массива из всех файлов в заданной папке
        File folder = new File(folderName);
        File[] files = folder.listFiles();

        // Коллекция всех классов
        ArrayList<String> classes = new ArrayList<>();
        // Коллекция классов из подпапок
        ArrayList<String> additionalClasses = new ArrayList<>();

        for (File file: files) {
            // Рекурся, выбираем классы из-под вложенных папок
            if (file.isDirectory()) {
                additionalClasses = getFiles(folderName + "\\" + file.getName());
                // Слияние двух коллекций
                classes.addAll(additionalClasses);
            } else {
                // Если расширение java, значит, это нужный нам класс, тогда добавляем его в коллекцию классов
                if (getFileExtension(file).equals("java")) {
                    String fileFolder = file.getPath().replaceAll("\\\\", ".");
                    fileFolder = fileFolder.replaceAll("D:.Java.ClassesSearcher.src.", "");
                    classes.add(fileFolder.substring(0,fileFolder.lastIndexOf(".")));
                }
            }
        }

        return classes;

    }

    // Метод для получения расширения файла
    private String getFileExtension(File file) {
        String fileName = file.getName();

        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        }

        return "";
    }
}
