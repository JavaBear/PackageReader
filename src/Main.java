import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter, please, full folder: ");
        // Пользователь вводит путь к папке
        String folder = scanner.nextLine();

        PackageReader pr = new PackageReader();
        // Получаем коллекцию всех файлов-классов
        ArrayList<String> classes = pr.getFiles(folder);

        // Создаем коллекцию всех объектов классов
        ArrayList<Object> allObjects = new ArrayList<>();

        //classes.add(0,"TestClasses.AutomatedObjects.AirPlane");

        // Цикл для создания объектов классов и добавления их в коллекцию
        for (String string: classes) {
            Class<?> objectClass = Class.forName(string);
            if (!objectClass.isAnnotation()) {
                Object object = objectClass.newInstance();

                Reflection.setClassFields(object);

                allObjects.add(object);
            }
        }

        System.out.println();

    }

}
