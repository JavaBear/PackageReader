public class TestAnnotationClass {

    @RandomInt(min = 0)
    public int value1;

    @RandomInt(min = 10, max = 50)
    public int value2;

    public int value3;

}
