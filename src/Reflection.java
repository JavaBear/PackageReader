import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Random;

public class Reflection {

    public static void setClassFields (Object object) throws IllegalAccessException {

        // Получаем объект класса Class
        Class<?> objectClass = object.getClass();

        // Получаем массив полей данного класса
        Field[] fields = objectClass.getDeclaredFields();

        // Устанавливаем значения в поля переданного объекта
        for (Field field: fields) {
            field.setAccessible(true);
            RandomInt annotation = field.getAnnotation(RandomInt.class);
            if (annotation == null) {
                if (field.getType() == int.class) {
                    Random random = new Random();
                    int value = random.nextInt(5);
                    field.setInt(object, value);
                } else if (field.getType() == double.class) {
                    double value = Math.random() + 50;
                    field.setDouble(object, value);
                } else if (field.getType() == boolean.class) {
                    boolean value = Math.random() < 0.5;
                    field.setBoolean(object, value);
                }
            } else {
                int value =new Random()
                        .nextInt(annotation.max() - annotation.min() + 1)
                        + annotation.min();
                field.setInt(object, value);
            }
        }
    }

}
